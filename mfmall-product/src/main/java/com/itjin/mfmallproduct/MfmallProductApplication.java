package com.itjin.mfmallproduct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
//可以被nacos管理，作为nacos的客户端
public class MfmallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(MfmallProductApplication.class, args);
    }

}
