package com.itjin.mfmallproduct.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itjin.mfmall.commom.utils.PageUtils;
import com.itjin.mfmallproduct.entity.AttrGroupEntity;

import java.util.Map;

/**
 * 属性分组表
 *
 * @author jin
 * @email lingjinlin@foxmail.com
 * @date 2021-09-03 14:22:59
 */
public interface AttrGroupService extends IService<AttrGroupEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

