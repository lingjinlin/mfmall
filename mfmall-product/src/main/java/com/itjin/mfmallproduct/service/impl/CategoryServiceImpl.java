package com.itjin.mfmallproduct.service.impl;

import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itjin.mfmall.commom.utils.PageUtils;
import com.itjin.mfmall.commom.utils.Query;

import com.itjin.mfmallproduct.dao.CategoryDao;
import com.itjin.mfmallproduct.entity.CategoryEntity;
import com.itjin.mfmallproduct.service.CategoryService;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 查询所有分类
     * @return
     */
    @Override
    public List<CategoryEntity> listWithThree() {

//        1.查询所有的分类：等同于select *
        List<CategoryEntity> entities = baseMapper.selectList(null);
//        2.组装成父子树状结构 Lambda 表达式
        List<CategoryEntity> levelOneMenue = entities.stream().filter(
            categoryEntity -> categoryEntity.getParentId()==0
        ).map((menue)->{
            //递归操作，关联子分类（2，3级分类）
            menue.setChildrens(getChildrens(menue,entities));
           return menue;
        }).collect(Collectors.toList());
//        2.1找到所有的一级分类
        return levelOneMenue;
    }

    /**
     * 递归查找指定分类的所有子分类
     * @param currenmenue
     * @param entities
     * @return
     */
    private List<CategoryEntity> getChildrens(CategoryEntity currenmenue, List<CategoryEntity> entities) {
        List<CategoryEntity> childrens= entities.stream().filter(
                //过滤出，当前菜单的所有匹配的菜单->当前id为子类的父id
                categoryEntity -> categoryEntity.getParentId() == currenmenue.getId()
        ).map((menue)->{
            menue.setChildrens( getChildrens(menue,entities) );
            return menue;
        }).collect(Collectors.toList());
        return  childrens;
    }

}