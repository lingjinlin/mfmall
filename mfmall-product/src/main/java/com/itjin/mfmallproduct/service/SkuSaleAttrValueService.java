package com.itjin.mfmallproduct.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itjin.mfmall.commom.utils.PageUtils;
import com.itjin.mfmallproduct.entity.SkuSaleAttrValueEntity;

import java.util.Map;

/**
 * sku销售属性值
 *
 * @author jin
 * @email lingjinlin@foxmail.com
 * @date 2021-09-03 14:22:58
 */
public interface SkuSaleAttrValueService extends IService<SkuSaleAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

