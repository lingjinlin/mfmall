package com.itjin.mfmallproduct.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itjin.mfmall.commom.utils.PageUtils;
import com.itjin.mfmallproduct.entity.UndoLogEntity;

import java.util.Map;

/**
 * 
 *
 * @author jin
 * @email lingjinlin@foxmail.com
 * @date 2021-09-03 14:22:58
 */
public interface UndoLogService extends IService<UndoLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

