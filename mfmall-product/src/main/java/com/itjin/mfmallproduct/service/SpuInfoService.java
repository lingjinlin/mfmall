package com.itjin.mfmallproduct.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itjin.mfmall.commom.utils.PageUtils;
import com.itjin.mfmallproduct.entity.SpuInfoEntity;

import java.util.Map;

/**
 * spu信息
 *
 * @author jin
 * @email lingjinlin@foxmail.com
 * @date 2021-09-03 14:22:58
 */
public interface SpuInfoService extends IService<SpuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

