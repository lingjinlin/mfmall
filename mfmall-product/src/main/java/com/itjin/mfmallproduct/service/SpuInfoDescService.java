package com.itjin.mfmallproduct.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itjin.mfmall.commom.utils.PageUtils;
import com.itjin.mfmallproduct.entity.SpuInfoDescEntity;

import java.util.Map;

/**
 * spu描述
 *
 * @author jin
 * @email lingjinlin@foxmail.com
 * @date 2021-09-03 14:22:59
 */
public interface SpuInfoDescService extends IService<SpuInfoDescEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

