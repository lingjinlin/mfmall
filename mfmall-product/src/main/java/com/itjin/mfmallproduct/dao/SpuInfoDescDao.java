package com.itjin.mfmallproduct.dao;

import com.itjin.mfmallproduct.entity.SpuInfoDescEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu描述
 * 
 * @author jin
 * @email lingjinlin@foxmail.com
 * @date 2021-09-03 14:22:59
 */
@Mapper
public interface SpuInfoDescDao extends BaseMapper<SpuInfoDescEntity> {
	
}
