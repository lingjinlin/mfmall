package com.itjin.mfmallproduct.dao;

import com.itjin.mfmallproduct.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author jin
 * @email lingjinlin@foxmail.com
 * @date 2021-09-03 14:22:58
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
