package com.itjin.mfmallproduct.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.itjin.mfmallproduct.entity.CategoryEntity;
import com.itjin.mfmallproduct.service.CategoryService;
import com.itjin.mfmall.commom.utils.PageUtils;
import com.itjin.mfmall.commom.utils.R;



/**
 * 商品类目
 * 权限验证不用shiro了
 * @author jin
 * @email lingjinlin@foxmail.com
 * @date 2021-09-04 11:17:35
 */
@RestController
@RequestMapping("mfmallproduct/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    /**
     * 查询出所有分类，以及树形结构
     */
    @RequestMapping("/list")
//    public R list(@RequestParam Map<String, Object> params){
//        PageUtils page = categoryService.queryPage(params);
//        return R.ok().put("page", page);
      public R list(){
        List<CategoryEntity> entities = categoryService.listWithThree();
        return R.ok().put("data", entities);

    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Integer id){
		CategoryEntity category = categoryService.getById(id);

        return R.ok().put("category", category);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody CategoryEntity category){
		categoryService.save(category);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody CategoryEntity category){
		categoryService.updateById(category);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Integer[] ids){
		categoryService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
