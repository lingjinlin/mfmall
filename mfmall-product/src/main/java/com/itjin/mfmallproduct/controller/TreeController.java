package com.itjin.mfmallproduct.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.itjin.mfmallproduct.entity.TreeEntity;
import com.itjin.mfmallproduct.service.TreeService;
import com.itjin.mfmall.commom.utils.PageUtils;
import com.itjin.mfmall.commom.utils.R;



/**
 * 
 * 权限验证不用shiro了
 * @author jin
 * @email lingjinlin@foxmail.com
 * @date 2021-09-04 11:17:35
 */
@RestController
@RequestMapping("mfmallproduct/tree")
public class TreeController {
    @Autowired
    private TreeService treeService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = treeService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{nodeId}")
    public R info(@PathVariable("nodeId") Integer nodeId){
		TreeEntity tree = treeService.getById(nodeId);

        return R.ok().put("tree", tree);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody TreeEntity tree){
		treeService.save(tree);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody TreeEntity tree){
		treeService.updateById(tree);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Integer[] nodeIds){
		treeService.removeByIds(Arrays.asList(nodeIds));

        return R.ok();
    }

}
